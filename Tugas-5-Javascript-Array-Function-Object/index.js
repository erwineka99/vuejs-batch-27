
//soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

//jawaban soal 1
var urutDaftarHewan =daftarHewan.sort();

for(var a = 0; a < urutDaftarHewan.length; a++) {
   console.log(urutDaftarHewan[a]);
} 



//soal 2 

//jawaban soal 2
function introduce(data){
    var result="Nama Saya "+data.name+", umur saya "+data.age+" tahun, alamat saya di "+data.address+", dan saya punya hobby yaitu "+data.hobby;

    return result;
}
 
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }

var perkenalan = introduce(data)
console.log(perkenalan) 


//soal 3

function hitung_huruf_vokal(kata){
    var huruf_vocal=["a","i","u","e","o"];
    var hitung=0;

    kata=kata.toLowerCase();
    for (var i = 0; i < kata.length; i++) {

        if(huruf_vocal.includes(kata.charAt(i))){
            hitung=hitung+1;
        }
    }
    return hitung;
}

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2)



function hitung(num){
    if(num==0){
        jumlah=-2;
    }
    if(num==1){
        jumlah=0;
    }
    if(num==2){
        jumlah=2;
    }
    if(num==3){
        jumlah=4;
    }
    if(num==5){
        jumlah=8;
    }

    return jumlah;
}



console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8