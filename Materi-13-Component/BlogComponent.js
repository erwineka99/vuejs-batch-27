export const BlogComponent={
    template:`
      <div>
       <b>Judul</b>  :  <slot name="title" v-bind:blogs="blogs">{{blogs[0].title}} </slot>
       <br>
       <b>Deskripsi </b> : <slot name="content" v-bind:blogs="blogs">{{ blogs[0].content }}</slot>
      </div>
    `,data(){
      return{
        blogs:[
        {
          title: 'Ini Judul 1',
          content: 'Ini konten 1'
        },
        {
          title: 'Ini Judul 2',
          content: 'Ini konten 2'
        }
      ]
      }
    }
  }