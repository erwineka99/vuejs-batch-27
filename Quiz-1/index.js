
//soal 1

function next_date(tanggal , bulan , tahun ){
    
    var currentDate = new Date(new Date(tahun+"-"+bulan+"-"+tanggal).getTime() + 24 * 60 * 60 * 1000);
    var day = currentDate.getDate()
    var month = currentDate.getMonth() + 1
    var year = currentDate.getFullYear()

    var bulan_text="";
    switch(month) {
        case 1:   { bulan_text="Januari"; break; }
        case 2:   { bulan_text="Februari"; break; }
        case 3:   { bulan_text="Maret"; break; }
        case 4:   { bulan_text="April"; break; }
        case 5:   { bulan_text="Mei"; break; }
        case 6:   { bulan_text="Juni"; break; }
        case 7:   { bulan_text="Juli"; break; }
        case 8:   { bulan_text="Agustus"; break; }
        case 9:   { bulan_text="September"; break; }
        case 10:  { bulan_text="Oktober"; break; }
        case 11:  { bulan_text="November"; break; }
        case 12:  { bulan_text="Desember";break; }
        default:  { bulan_text="invalid"; }}



    var hasil=day+"-"+bulan_text+"-"+year;




    console.log(hasil);
}



var tanggal = 29
var bulan = 2
var tahun = 2020

next_date(tanggal , bulan , tahun ) // output : 1 Maret 2020


var tanggal = 28
var bulan = 2
var tahun = 2021

next_date(tanggal , bulan , tahun ) // output : 1 Maret 2021


var tanggal = 31
var bulan = 12
var tahun = 2020

next_date(tanggal , bulan , tahun )




//soal 2


function jumlah_kata(kalimat){
    var kata=kalimat.split(" ");
    var count=0;
    
    for(var i=0;i<kata.length;i++){
        if(kata[i]!=''){
            count=count+1;
        }
       
    }

    console.log(count);
}


var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok "
var kalimat_2 = "Saya Iqbal"


jumlah_kata(kalimat_1) // 6
jumlah_kata(kalimat_2) // 2