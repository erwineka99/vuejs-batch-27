
//soal 1

const luas_persegipanjang = (panjang,lebar) => {
    //function
    let luas=panjang*lebar;

    console.log(luas);
}

const keliling_persegipanjang = (panjang,lebar) => {
    //function
    const pengali=2;

    let keliling=(panjang*pengali)+(lebar*pengali);

    console.log(keliling);


}

luas_persegipanjang(6,5);

keliling_persegipanjang(5,4);


//Soal 2

const newFunction =(firstName, lastName)=>{
    return {
      fullName: function(){
        console.log(`${firstName} ${lastName}`)
      }
    }
  }
   
  //Driver Code 
  newFunction("William", "Imoh").fullName() 
  

  //soal 3
  const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  }

  const {firstName, lastName,address,hobby} = newObject;


  console.log(firstName, lastName, address, hobby)


  //soal 4

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

let combined = [...west, ...east]

//Driver Code
console.log(combined)



//soal 5

const planet = "earth" 
const view = "glass" 
var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 

var after=`Lorem ${view}dolor sit amet, consectetur adipiscing elit, ${planet}`

console.log(after);
