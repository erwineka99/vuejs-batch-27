var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]

var detik="10000";


// readBooksPromise(detik,books[0])
//   .then((a) => {
//     readBooksPromise(a,books[1])
//     .then((b)=>{
//         readBooksPromise(b,books[2]).then((c)=>{
//             readBooksPromise(c,books[3])
//         })
//     })
//   })
//   .catch(error => console.log(error));

function recurse(detik=10000,i=0){
    readBooksPromise(detik,books[i])
    .then((a) => {
        if(i<books.length){
            recurse(a,i);
        }
    })
    i++;
}

recurse();