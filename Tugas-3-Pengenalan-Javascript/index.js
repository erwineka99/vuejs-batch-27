// Soal 1


var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

//output saya senang belajar JAVASCRIPT

//jawaban soal 1
var gabungan = pertama.substr(0,4) + " "+ pertama.substr(12,6)+" "+kedua.substr(0,7)+" "+kedua.substr(8,10).toUpperCase();
console.log(gabungan);


//Soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

//jawaban soal 2
// 10+(2*4)+6
var jawaban_perhitungan=parseInt(kataPertama)+(parseInt(kataKedua)*parseInt(kataKetiga))+parseInt(kataKeempat);

console.log(jawaban_perhitungan);


//jawaban soal 3

var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(3, 15); 
var kataKetiga = kalimat.substring(14, 18); 
var kataKeempat = kalimat.substring(19, 24); 
var kataKelima = kalimat.substring(25, 36); 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);