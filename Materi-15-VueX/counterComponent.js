export const counterComponent= {
    template:
    `
    <div>
        <p> State Pada component counter    </p>{{ counter }}
    </div>
    `,
    computed :{
        counter(){
            return this.$store.getters.counter
        }
    }
}